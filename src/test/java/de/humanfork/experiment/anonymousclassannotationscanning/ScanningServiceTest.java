package de.humanfork.experiment.anonymousclassannotationscanning;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Set;

import org.junit.jupiter.api.Test;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

public class ScanningServiceTest {

    /**
     * Configure and build a Reflections scanner to find the fields annotated with {@link PredefinedOptionCategoryRegistration} in the given root packages.
     * @return the scanner
     */
    private Reflections buildReflectionsScanner() {
        try {
            return new Reflections(
                    new ConfigurationBuilder()
                            .setUrls(ClasspathHelper.forPackage(DemoUsageService.class.getPackageName()))
                            .setScanners(
                                    new SubTypesScanner(false),
                                    new TypeAnnotationsScanner()));
        } catch (RuntimeException e) {
            throw new RuntimeException(
                    "error while configuring Reflections scanner for PredefinedOption Annotation scann",
                    e);
        }
    }

    @Test
    public void testFindAnnotatedClasses() throws Exception {

        Reflections reflections = new Reflections(
                new ConfigurationBuilder()
                        .setUrls(ClasspathHelper.forPackage(DemoUsageService.class.getPackageName()))
                        .setScanners(
                                new AnnonymousClassScanner(),
                                new SubTypesScanner(false)));

        Set<Class<?>> result = reflections.getTypesAnnotatedWith(MarkerAnnotation.class);
        assertThat(result).hasSize(1);
        Class<?> foundClass = result.iterator().next();
        assertThat(foundClass.getName())
                .isEqualTo("de.humanfork.experiment.anonymousclassannotationscanning.DemoUsageService$1");

        assertThat(foundClass.isAnonymousClass());
        MarkerAnnotation annotation = foundClass.getAnnotatedSuperclass().getDeclaredAnnotation(MarkerAnnotation.class);
        assertThat(annotation).isNotNull();
        assertThat(annotation.value()).isEqualTo("Test");

    }

}
