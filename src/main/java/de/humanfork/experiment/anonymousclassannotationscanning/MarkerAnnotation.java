package de.humanfork.experiment.anonymousclassannotationscanning;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(ElementType.TYPE_USE)
public @interface MarkerAnnotation {
    String value();
}
