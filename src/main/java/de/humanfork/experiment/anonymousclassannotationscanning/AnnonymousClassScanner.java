package de.humanfork.experiment.anonymousclassannotationscanning;

import java.lang.annotation.Annotation;

import org.reflections.Store;
import org.reflections.scanners.AbstractScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.Utils;

public class AnnonymousClassScanner extends AbstractScanner {

    @Override
    public void scan(Object cls, Store store) {
        String className = getMetadataAdapter().getClassName(cls);

        try {
            Class<?> c = Class.forName(className);
            if (c.isAnonymousClass()) {
                for (Annotation ann : c.getAnnotatedSuperclass().getAnnotations()) {
                    store.put(Utils.index(TypeAnnotationsScanner.class), ann.annotationType().getName(), className);
                }

            }
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        
    }

}
