Experiment: how to find a anonymous class by annotation driven class path scan
==============================================================================

WARNING: see [JDK-8059531: Anonymous inner class unable to access in-scope variables/methods when annotated](https://bugs.openjdk.java.net/browse/JDK-8059531) and my git project [experiment-bug-jdk8059531](experiment-bug-jdk8059531)

```
public class UsageService {

	public void doSomething() {
        this.test(new @MarkerAnnotation(value="Test") Object() {
            String content = "myContent";
        });
    }
}
```

class path scanning library

```
<dependency>
	<groupId>org.reflections</groupId>
    <artifactId>reflections</artifactId>
    <version>0.9.12</version>
</dependency>
```

The trick is an own `org.reflections.scanners.Scanner`:
_(thanks to [Eugene](https://stackoverflow.com/questions/63996281/find-anonymous-classes-by-annotation/63998567#63998567))_

```
import java.lang.annotation.Annotation;

import org.reflections.Store;
import org.reflections.scanners.AbstractScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.Utils;

public class AnnonymousClassScanner extends AbstractScanner {

    @Override
    public void scan(Object cls, Store store) {
        String className = getMetadataAdapter().getClassName(cls);

        try {
            Class<?> c = Class.forName(className);
            if (c.isAnonymousClass()) {
                for (Annotation ann : c.getAnnotatedSuperclass().getAnnotations()) {
                    store.put(Utils.index(TypeAnnotationsScanner.class), ann.annotationType().getName(), className);
                }

            }
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        
    }

}
```



Important: the annotation must have `ElementType.TYPE_USE`.
Eclipse will even compile without `@Target` annotation, but Maven will fail.
```
@Retention(RUNTIME)
@Target(ElementType.TYPE_USE)
public @interface MarkerAnnotation {
    String value();
}
```


Resources
---------

* Stackoverflow: [Find anonymous classes by annotation](https://stackoverflow.com/questions/63996281/find-anonymous-classes-by-annotation/63998567#63998567)
